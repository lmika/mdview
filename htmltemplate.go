package main

import (
    "html/template"
    "bytes"
)

type HtmlTemplate struct {
    tmpl    *template.Template
}

func NewHtmlTemplate() *HtmlTemplate {
    return &HtmlTemplate{template.Must(template.New("default").Parse(defaultTemplate))}
}

func (ht *HtmlTemplate) Render(mdContent string) string {
    res := new(bytes.Buffer)
    err := ht.tmpl.Execute(res, map[string]interface{} {
        "mdContent": template.HTML(mdContent),
    })

    if err != nil {
        return "<p>Error rendering HTML: " + err.Error() + "</p>"
    } else {
        return res.String()
    }
}


var defaultTemplate = `<!DOCTYPE html>
<head>
</head>
<body>
    {{.mdContent}}

<script>
</script>
</body>
</html>
`

// Bootstrap JS script
var bootstrapJsScript = `
function clickLink(ev) {
    ev.preventDefault();
    mdMessage("open", ev.target.getAttribute("href"));
};

var links = document.querySelectorAll("a");
for (var i = 0; i < links.length; i++) {
    links[i].addEventListener("click", clickLink);
}
`