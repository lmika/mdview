package main

import (
    "log"
    "path/filepath"
    "strings"

    "github.com/gotk3/gotk3/glib"
    "github.com/gotk3/gotk3/gtk"
    "github.com/lmika-bom/go-webkit2/webkit2"   // TEMP
//    "github.com/sqs/gojs"
)


/**
 * The main window.
 */
type MainWindow struct {
    window      *gtk.Window
    webview     *webkit2.WebView
    template    *HtmlTemplate
    currUrl     string
}


func NewMainWindow() (*MainWindow, error) {
    window, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
    if err != nil {
        return nil, err
    }

    window.SetTitle("Markdown Viewer")
    window.Connect("destroy", gtk.MainQuit)
    window.SetSizeRequest(800, 600)

    swin, err := gtk.ScrolledWindowNew(nil, nil)
    if err != nil {
        return nil, err
    }

    swin.SetPolicy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    //swin.SetShadowType(gtk.SHADOW_IN)

    webview := webkit2.NewWebView()
    webview.Settings().SetEnableWriteConsoleMessagesToStdout(true)

    swin.Add(webview)
    window.Add(swin)

    template := NewHtmlTemplate()

    mw := &MainWindow{window, webview, template, ""}

    webview.Connect("load-changed", func(o *glib.Object, state webkit2.LoadEvent) bool {
        url := webview.URI()

        // Intercept navigation requests and either render a markdown page or open a browser
        if url == mw.currUrl {
            // If loading the current URL, allow to proceed
            return false
        } else if strings.HasPrefix(url, "file://") && (strings.HasSuffix(url, "md")) || (strings.HasSuffix(url, "markdown")) {
            webview.StopLoading()
            log.Println("RENDER: ", url)

            return true
        } else {
            webview.StopLoading()
            log.Println("OPEN: ", url)

            return true
        }
    })

    /*

    // Setup message routing
    webview.Connect("load-changed", func(_ *glib.Object, i int) {
        loadEvent := webkit2.LoadEvent(i)
        if loadEvent == webkit2.LoadFinished {
            webview.RunJavaScript("window", func(windowVal *gojs.Value, err error) {
                jsCtx := mw.webview.JavaScriptGlobalContext()
                if err != nil {
                    log.Println("Error installing JS hooks: ", err)
                }

                windowObj := jsCtx.ToObjectOrDie(windowVal)

                err = mw.installJSHooks(jsCtx, windowObj)
                if err != nil {
                    log.Println("Error installing JS hooks: ", err)
                }

                webview.RunJavaScript(bootstrapJsScript, func(_ *gojs.Value, err error) {
                    if (err != nil) {
                        log.Println("JS Error: ", err)
                    }
                })
            })
        }
    })
    */

    return mw, nil
}

// Add custom Java Script hooks to intercept link clicks, etc
/*
func (mw *MainWindow) installJSHooks(jsCtx *gojs.Context, jsWindowObj *gojs.Object) error {
    log.Println(".. creating function")
    jsMsgPassObj := jsCtx.NewFunctionWithCallback(mw.mdMessageFunc)

    log.Println(".. setting function on window")
    return jsCtx.SetProperty(jsWindowObj, "mdMessage", jsMsgPassObj.ToValue(),
        gojs.PropertyAttributeReadOnly | gojs.PropertyAttributeDontDelete)
}

// Function which receives messages from JS
func (mw *MainWindow) mdMessageFunc(ctx *gojs.Context, obj *gojs.Object, thisObject *gojs.Object, arguments []*gojs.Value) (ret *gojs.Value) {
    if len(arguments) == 0 {
        return nil
    }

    eventName := arguments[0].String()

    if (eventName == "open") && (len(arguments) >= 2) {
        log.Println("OPEN: ", arguments[1].String())
    }

    return nil
}
*/

// Loads the rendered HTML.  This will be the rendered Markdown file.
func (mw *MainWindow) RenderHtml(mdContent string, filename string) {
    basename := filepath.Base(filename)

    htmlContent := mw.template.Render(mdContent)
    log.Println(htmlContent)

    mw.currUrl = "file://" + filename
    mw.webview.LoadHTML(htmlContent, mw.currUrl)
    mw.window.SetTitle(basename + " - Markdown Viewer")
}

func (mw *MainWindow) Show() {
    mw.window.ShowAll()
}