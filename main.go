package main

import (
    "log"

    "fmt"
    "os"
    "flag"
    "io/ioutil"
    "path/filepath"

    "github.com/russross/blackfriday"

    "github.com/gotk3/gotk3/gtk"
)

func main() {
    // Read the flags
    flag.Parse()
    if flag.NArg() == 0 {
        fmt.Fprintln(os.Stderr, "Usage: mdview MARKDOWN")
        os.Exit(1)
    }

    // Parse the markdown file
    filename, err := filepath.Abs(flag.Arg(0))
    if err != nil {
        log.Fatal(err)
    }

    html, err := renderMarkdown(filename)
    if err != nil {
        log.Fatal(err)
    }

    // Show it in the viewer
    gtk.Init(nil)

    mw, err := NewMainWindow()
    if err != nil {
        log.Fatal(err)
    }
    
    mw.RenderHtml(html, filename)
    mw.Show()

    gtk.Main()
}

// Renders the markdown file
func renderMarkdown(filename string) (string, error) {
    file, err := os.Open(filename)
    if err != nil {
        return "", err
    }
    defer file.Close()

    mdSrc, err := ioutil.ReadAll(file)
    if err != nil {
        return "", err
    }

    htmlFlags := 0
    exts := blackfriday.EXTENSION_TABLES |
            blackfriday.EXTENSION_FENCED_CODE |
            blackfriday.EXTENSION_AUTOLINK
    renderer := blackfriday.HtmlRenderer(htmlFlags, "", "")

    return string(blackfriday.Markdown(mdSrc, renderer, exts)), nil
}