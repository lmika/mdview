package main

import (
    "io"
    "io/ioutil"

    "github.com/russross/blackfriday"
)

// A markdown renderer
type MarkdownRenderer interface {
    
    // Render the markdown to HTML
    Render(r io.Reader) (string, error)
}


type BlackFridayRenderer struct {
    renderer    blackfriday.Renderer
}

func NewBlackFridayRenderer() *BlackFridayRenderer {
    htmlFlags := 0
    renderer := blackfriday.HtmlRenderer(htmlFlags, "", "")

    return &BlackFridayRenderer{renderer}
}

// Render the markdown to HTML
func (bfr *BlackFridayRenderer) Render(r io.Reader) (string, error) {
    mdSrc, err := ioutil.ReadAll(r)
    if err != nil {
        return "", err
    }

    exts := blackfriday.EXTENSION_TABLES |
            blackfriday.EXTENSION_FENCED_CODE |
            blackfriday.EXTENSION_AUTOLINK

    return string(blackfriday.Markdown(mdSrc, bfr.renderer, exts)), nil
}